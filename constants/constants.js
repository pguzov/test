export const titleText = {
  personalInformation: 'Персональные данные',
  delivery: 'Адрес для доставки',
  deliveryAdress: 'Укажите адрес для доставки',
  dateTimeDelivery: 'Дата и время доставки',
  costOrders: 'Сумма заказа',
  paymentsMethods: 'Способы оплаты',
  productReceipt: 'Приложить товарный чек'
}