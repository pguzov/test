import React from "react";
import {
  View,
  Text,
  Switch,
} from "react-native";
import { titleText } from "../../constants/constants";
import { stylesReceipt } from "./stylesReceipt";

const Receipt = ({ isEnabled, toggleSwitch }) => {
  return (
    <View style={stylesReceipt.container}>
      <Text style={stylesReceipt.title}>{titleText.productReceipt}</Text>
      <Switch
        trackColor={{ false: "#767577", true: "#81b0ff" }}
        thumbColor={isEnabled ? "#f5dd4b" : "#f4f3f4"}
        ios_backgroundColor='#3e3e3e'
        onValueChange={toggleSwitch}
        value={isEnabled}
      />
    </View>
  );
};

export default Receipt;
