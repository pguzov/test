import { StyleSheet } from "react-native";

export const stylesReceipt = StyleSheet.create({
  container: {
    marginTop: 40,
    paddingLeft: 20,
    width: "100%",
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    backgroundColor: "white",
    height: 50,
    borderBottomColor: "grey",
    borderBottomWidth: 0.4,
    paddingRight: 10,
  },
  title: {
    fontSize: 16,
  },
});
