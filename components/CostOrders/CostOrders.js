import React from "react";
import {
  View,
  Text,
} from "react-native";
import { styles } from "../ModalOrders/stylesOrders";
import { stylesOrders } from "./stylesOrders";

const CostOrders = () => {
  return (
    <View>
      <View style={stylesOrders.container}>
        <Text style={stylesOrders.title}>Стоимость товаров</Text>
        <Text style={stylesOrders.titleCost}>1470р</Text>
      </View>
      <View style={stylesOrders.container}>
        <Text style={stylesOrders.title}>Стоимость доставки</Text>
        <Text style={stylesOrders.titleCost}>0р</Text>
      </View>
      <View style={stylesOrders.container}>
        <Text style={stylesOrders.title}>Итого</Text>
        <Text style={stylesOrders.titleCost}>1470р</Text>
      </View>
    </View>
  );
};

export default CostOrders;
