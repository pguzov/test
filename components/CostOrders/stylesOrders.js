import { StyleSheet } from "react-native";

export const stylesOrders = StyleSheet.create({
  container: {
    paddingLeft: 20,
    width: "100%",
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    backgroundColor: "white",
    height: 50,
    borderBottomColor: "grey",
    borderBottomWidth: 0.4,
  },
  title: {
    fontSize: 16,
  },
  titleCost: {
    fontSize: 16,
    opacity: 0.6,
    paddingRight: 20,
  },
});
