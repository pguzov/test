import React, { useState } from "react";
import {
  SafeAreaView,
  View,
  Text,
  TextInput,
  Button,
  ScrollView,
} from "react-native";
import DateTimePicker from "@react-native-community/datetimepicker";
import { AntDesign } from "@expo/vector-icons";
import { styles } from "./stylesOrders";
import PaymentBox from "../PaymentBox/PaymentBox";
import { titleText } from "../../constants/constants";
import ChooseDelivery from "../ChooseDelivery/ChooseDelivery";
import AdressDelivery from "../AdressDelivery/AdressDelivery";
import CostOrders from "../CostOrders/CostOrders";
import Receipt from "../Receipt/Receipt";

const ModalOrders = ({ setIsOpen }) => {
  const [checked, setChecked] = React.useState("first");

  const [date, setDate] = useState(new Date());
  const [mode, setMode] = useState("date");
  const [show, setShow] = useState(false);

  const [isEnabled, setIsEnabled] = useState(false);

  const toggleSwitch = () => setIsEnabled((previousState) => !previousState);

  const handlePressBack = () => {
    setIsOpen(false);
  };

  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate;
    setShow(true);
    setDate(currentDate);
  };

  const showMode = (currentMode) => {
    setShow(true);
    setMode(currentMode);
  };

  const showDatepicker = () => {
    showMode("date");
  };

  return (
    <View style={styles.mainContainer}>
      <SafeAreaView style={styles.container}>
        <ScrollView>
          <View style={styles.header}>
            <AntDesign
              name='left'
              size={24}
              color='orange'
              onPress={handlePressBack}
            />
            <Text style={styles.titleHeader}>Корзина</Text>
          </View>
          <View style={styles.titleContainer}>
            <Text style={styles.title}>{titleText.personalInformation}</Text>
          </View>
          <View style={styles.inputBox}>
            <View style={styles.inputContainer}>
              <TextInput
                placeholder='Имя'
                keyboardType='default'
                style={styles.input}
              />
            </View>
            <View style={styles.inputContainer}>
              <TextInput
                placeholder='Email'
                keyboardType='email-address'
                style={styles.input}
              />
            </View>
            <View style={styles.inputContainer}>
              <TextInput
                placeholder='Телефон'
                keyboardType='name-phone-pad'
                style={styles.lastInput}
              />
            </View>
          </View>
          <View style={styles.titleContainer}>
            <Text style={styles.title}>{titleText.delivery}</Text>
          </View>
          <ChooseDelivery checked={checked} setChecked={setChecked} />
          <View style={styles.titleContainer}>
            <Text style={styles.title}>{titleText.deliveryAdress}</Text>
          </View>
          <AdressDelivery />
          <View style={styles.titleContainer}>
            <Text style={styles.title}>{titleText.dateTimeDelivery}</Text>
          </View>
          <View>
            {show ? (
              <View>
                <DateTimePicker
                  style={styles.picker}
                  testID='dateTimePicker'
                  value={date}
                  mode={mode}
                  is24Hour={true}
                  onChange={onChange}
                />
              </View>
            ) : (
              <View style={styles.dateContainer} onTouchEnd={showDatepicker}>
                <Text style={styles.titleText}>Выберите дату и время</Text>
                <AntDesign
                  style={styles.icon}
                  name='right'
                  size={16}
                  color='black'
                />
              </View>
            )}
          </View>
          <View>
            <TextInput
              placeholder='Промокод'
              keyboardType='default'
              style={styles.soloInput}
            />
          </View>
          <View style={styles.titleContainer}>
            <Text style={styles.title}>{titleText.costOrders}</Text>
          </View>
          <CostOrders />
          <View style={styles.titleContainer}>
            <Text style={styles.title}>{titleText.paymentsMethods}</Text>
          </View>
          <PaymentBox />
          <View>
            <Receipt isEnabled={isEnabled} toggleSwitch={toggleSwitch} />
          </View>
          <View>
            <TextInput
              keyboardType='default'
              placeholder='Комментарий к заказу'
              style={styles.soloInput}
            />
          </View>
        </ScrollView>
      </SafeAreaView>
      <View style={styles.button}>
        <Button title='Заказать' color='white' size='100%' />
      </View>
    </View>
  );
};

export default ModalOrders;
