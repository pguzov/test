import React, { useState } from "react";
import { Text, SafeAreaView, Modal, View, Button } from "react-native";
import ModalOrders from "./ModalOrders/ModalOrders";

const MainPage = () => {
  const [isOpen, setIsOpen] = useState(false);

  const handlePress = () => {
    setIsOpen(true);
  };

  return (
    <SafeAreaView style={{ backgroundColor: "black", flex: 1 }}>
      <Text style={{ color: "white" }}>MainPage</Text>
      <View>
        <Modal visible={isOpen} animationType='slide'>
          <ModalOrders setIsOpen={setIsOpen} />
        </Modal>
      </View>
      <View>
        <Button
          title='Tap Me'
          style={{ color: "white" }}
          onPress={handlePress}
        />
      </View>
    </SafeAreaView>
  );
};

export default MainPage;
