import React, { useState } from "react";
import { View, Text } from "react-native";
import { RadioButton } from "react-native-paper";
import { styles } from "../ModalOrders/stylesOrders";

const PaymentBox = () => {
  const [checked, setChecked] = useState("first");

  return (
    <View>
      <View style={styles.radioBox} onTouchEnd={() => setChecked("first")}>
        <View style={styles.radio}>
          <RadioButton
            value='first'
            status={checked === "first" ? "checked" : "unchecked"}
          />
        </View>
        <Text>Оплата при получении</Text>
      </View>
      <View style={styles.radioBox} onTouchEnd={() => setChecked("second")}>
        <View style={styles.radio}>
          <RadioButton
            value='second'
            status={checked === "second" ? "checked" : "unchecked"}
          />
        </View>
        <Text>Банковской картой онлайн</Text>
      </View>
      <View style={styles.radioBox} onTouchEnd={() => setChecked("third")}>
        <View style={styles.radio}>
          <RadioButton
            value='third'
            status={checked === "third" ? "checked" : "unchecked"}
          />
        </View>
        <Text>Apple Pay</Text>
      </View>
    </View>
  );
};

export default PaymentBox;
