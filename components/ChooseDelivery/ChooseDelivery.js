import React from "react";
import {
  View,
  Text,
} from "react-native";
import { RadioButton } from "react-native-paper";
import { styles } from "../ModalOrders/stylesOrders";

const ChooseDelivery = ({ checked, setChecked }) => {
  return (
    <View>
      <View style={styles.radioBox} onTouchEnd={() => setChecked("first")}>
        <View style={styles.radio}>
          <RadioButton
            value='first'
            status={checked === "first" ? "checked" : "unchecked"}
          />
        </View>
        <Text style={styles.contentTitle}>Новый адрес</Text>
      </View>
      <View style={styles.radioBox} onTouchEnd={() => setChecked("second")}>
        <View style={styles.radio}>
          <RadioButton
            value='second'
            status={checked === "second" ? "checked" : "unchecked"}
          />
        </View>
        <Text style={styles.contentTitle}>Самовывоз</Text>
      </View>
    </View>
  );
};

export default ChooseDelivery;
