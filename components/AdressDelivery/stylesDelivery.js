import { StyleSheet } from "react-native";

export const stylesDelivery = StyleSheet.create({
  container: {
    paddingLeft: 20,
    width: "100%",
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    backgroundColor: "white",
    height: 50,
    borderBottomColor: "grey",
    borderBottomWidth: 0.4,
  },
  title: {
    fontSize: 16,
  },
  input: {
    width: "50%",
    fontSize: 16,
    textAlign: "right",
    paddingRight: 20,
  },
});
