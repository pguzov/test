import React from "react";
import { View, Text, TextInput } from "react-native";
import { stylesDelivery } from "./stylesDelivery";

const AdressDelivery = ({}) => {
  return (
    <View>
      <View style={stylesDelivery.container}>
        <Text style={stylesDelivery.title}>Город</Text>
        <TextInput
          placeholder='Санкт-Петербург'
          keyboardType='default'
          style={stylesDelivery.input}
        />
      </View>
      <View style={stylesDelivery.container}>
        <Text style={stylesDelivery.title}>Улица и дом</Text>
        <TextInput
          placeholder='Улица и номер дома'
          keyboardType='default'
          style={stylesDelivery.input}
        />
      </View>
      <View style={stylesDelivery.container}>
        <Text style={stylesDelivery.title}>Квартира</Text>
        <TextInput
          placeholder='Квартира или офис'
          keyboardType='default'
          style={stylesDelivery.input}
        />
      </View>
    </View>
  );
};

export default AdressDelivery;
