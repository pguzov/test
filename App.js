import { StatusBar } from "expo-status-bar";
import { StyleSheet, View, SafeAreaView } from "react-native";
import MainPage from "./components/MainPage";

export default function App() {
  return (
    <SafeAreaView style={styles.container}>
      <MainPage />
      <StatusBar style='auto' />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#fff",
    flex: 1,
  },
  modals: {
    borderRadius: 10,
    top: 10,
    flex: 1,
    backgroundColor: "grey",
  },
  title: {
    fontSize: 30,
    height: "60px",
    width: "100%",
    color: "orange",
  },
});
